func private @print_memref_f32(memref<*xf32>)

//  // C += A * B.
//  func @matmul(%A: memref<2048x2048xf32>, %B: memref<2048x2048xf32>, %C: memref<2048x2048xf32>) {
//    affine.for %arg3 = 0 to 2048 {
//      affine.for %arg4 = 0 to 2048 {
//        affine.for %arg5 = 0 to 2048 {
//          %a = affine.load %A[%arg3, %arg5] : memref<2048x2048xf32>
//          %b = affine.load %B[%arg5, %arg4] : memref<2048x2048xf32>
//          %ci = affine.load %C[%arg3, %arg4] : memref<2048x2048xf32>
//          %p = mulf %a, %b : f32
//          %co = addf %ci, %p : f32
//          affine.store %co, %C[%arg3, %arg4] : memref<2048x2048xf32>
//        }
//      }
//    }
//    return
//  }
//  
//  func @main() {
//    %A = memref.alloc() : memref<2048x2048xf32>
//    %B = memref.alloc() : memref<2048x2048xf32>
//    %C = memref.alloc() : memref<2048x2048xf32>
//  
//    %cf0 = constant 0.00000e+00 : f32
//    %cf1 = constant 1.00000e+00 : f32
//  
//    linalg.fill(%cf1, %A) : f32, memref<2048x2048xf32>
//    linalg.fill(%cf1, %B) : f32, memref<2048x2048xf32>
//    linalg.fill(%cf0, %C) : f32, memref<2048x2048xf32>
//  
//    call @matmul(%A, %B, %C) : (memref<2048x2048xf32>, memref<2048x2048xf32>, memref<2048x2048xf32>) -> ()
//    %l = memref.cast %C : memref<2048x2048xf32> to memref<*xf32>
//    call @print_memref_f32(%l): (memref<*xf32>) -> ()
//    // call @print_memref_f32(%C): (memref<2048x2048xf32>) -> ()
//    memref.dealloc %A : memref<2048x2048xf32>
//    memref.dealloc %B : memref<2048x2048xf32>
//    memref.dealloc %C : memref<2048x2048xf32>
//    return
//  }

// C += A * B.
func @matmul(%A: memref<512x512xf32>, %B: memref<512x512xf32>, %C: memref<512x512xf32>) {
  affine.for %arg3 = 0 to 512 {
    affine.for %arg4 = 0 to 512 {
      affine.for %arg5 = 0 to 512 {
        %a = affine.load %A[%arg3, %arg5] : memref<512x512xf32>
        %b = affine.load %B[%arg5, %arg4] : memref<512x512xf32>
        %ci = affine.load %C[%arg3, %arg4] : memref<512x512xf32>
        %p = mulf %a, %b : f32
        %co = addf %ci, %p : f32
        affine.store %co, %C[%arg3, %arg4] : memref<512x512xf32>
      }
    }
  }
  return
}

func @main() {
  %A = memref.alloc() : memref<512x512xf32>
  %B = memref.alloc() : memref<512x512xf32>
  %C = memref.alloc() : memref<512x512xf32>

  %cf0 = constant 0.00000e+00 : f32
  %cf1 = constant 1.00000e+00 : f32

  linalg.fill(%cf0, %A) : f32, memref<512x512xf32>
  linalg.fill(%cf0, %B) : f32, memref<512x512xf32>
  linalg.fill(%cf1, %C) : f32, memref<512x512xf32>
  affine.for %arg3 = 0 to 512 {
    affine.store %cf1, %A[%arg3, %arg3] : memref<512x512xf32>
    affine.store %cf1, %B[%arg3, %arg3] : memref<512x512xf32>
  }

  call @matmul(%A, %B, %C) : (memref<512x512xf32>, memref<512x512xf32>, memref<512x512xf32>) -> ()
  %l = memref.cast %C : memref<512x512xf32> to memref<*xf32>
  call @print_memref_f32(%l): (memref<*xf32>) -> ()
  // call @print_memref_f32(%C): (memref<512x512xf32>) -> ()
  memref.dealloc %A : memref<512x512xf32>
  memref.dealloc %B : memref<512x512xf32>
  memref.dealloc %C : memref<512x512xf32>
  return
}
