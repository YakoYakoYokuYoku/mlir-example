MLIR GEMM demo
==============

The following is an MLIR example of a GEMM function based on a guide written by @bondhugula (link `here <https://github.com/bondhugula/llvm-project/blob/dc76de2340db5acaf59b33906f6d2684f50246de/mlir/docs/HighPerfCodeGen.md>`_).

Instructions
------------

Optimize the ``test.mlir`` file to use it with a runner::

  $ mlir-opt -convert-linalg-to-loops -lower-affine -convert-scf-to-std -convert-memref-to-llvm -convert-std-to-llvm test.mlir > test.opt.mlir

And then, for example, run it on the CPU (WARNING, it prints a 512x512 matrix, lots of output)::

  $ mlir-cpu-runner -O3 -e main -entry-point-result=void -shared-libs=/usr/lib64/libmlir_runner_utils.so < test.opt.mlir
